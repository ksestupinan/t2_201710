package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {
	
	private NodoDoble<T> list;
	private NodoDoble<T> actuald;
	private int listSize;
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		
		return new Iterator<T>()
				{
					NodoDoble<T> act = null;

					public boolean hasNext()
					{
						if(listSize == 0)
							return false;
						if(act == null)
							return true;

						return act.getNext()!=null;
					}

					public T next()
					{
						if(act == null)
						{
							act = list;
						}
						else
						{
							act = act.getNext();
						}
						return act.getItem();
					}

					
					

					
				};
	
	}
	public ListaDobleEncadenada()
	{
		list = null;
		actuald = null;
		listSize = 0;
		
	}
	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nNodo=new NodoDoble<T>(elem);
		if(list == null)
		{
			list=nNodo;
			actuald = nNodo;
		}
		
		else
		{
			NodoDoble<T>  actual = list;
			while(actual.getNext() != null)
			{
				actual = actual.getNext();
			}
			actual.setNext(nNodo);
			nNodo.setPrevious(actual);
			actuald = nNodo;
		}
		listSize++;
		
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		NodoDoble<T>  actual = list;
		int posi = 0;
		while(actual.getNext() != null && posi <= pos)
		{
			actual = actual.getNext();
			posi++;
		};

		return (T) actual.getItem();
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return listSize;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actuald.getItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		NodoDoble<T>  actual = new NodoDoble (actuald);
		boolean d =false;
		if(actual.getNext() != null)
		{
			actual.getNext();
			d = true;
		}

		return d;
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		
		NodoDoble<T>  actual = new NodoDoble (actuald);
		boolean d = false;
		if(actual.getPrevious() != null)
		{
			actual.getPrevious();
			d = true;
		}

		return d;
	}

}
