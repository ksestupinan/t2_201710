package model.data_structures;

public class Nodo <T>{
	private Nodo<T> next;
	private T item;
	
	public Nodo(T it)
	{
		next=null;
		this.item=it;
		
	}
	public Nodo<T> getNext()
	{
		return next;
	}
	public void setnext(Nodo<T> elem)
	{
		this.next=elem;
	}
	public T getitem()
	{
		return item;
	}
	public void setitem(T i)
	{
		this.item=i;
	}
	
	
}