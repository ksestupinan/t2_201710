package model.data_structures;

public class NodoDoble<T> 
{
	private NodoDoble<T> next;
	private NodoDoble<T> previous;
	private T item;

	public NodoDoble(T it)
	{
		next = null;
		previous = null;
		this.item = it;
	}
	
	public NodoDoble<T> getNext()
	{
		return next;
	}
	public NodoDoble<T> getPrevious()
	{
		return previous;
	}
	public void setNext( NodoDoble<T> elem)
	{
		this.next=elem;
	}
	public void setPrevious( NodoDoble<T> elem)
	{
		this.previous=elem;
	}
	public T getItem()
	{
		return item;
	}
	public void setItem( T i)
	{
		this.item=i;
	}
}
