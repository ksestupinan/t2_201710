package Test;
import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class ListaDoblementeEncadenadaTest extends TestCase
{
	private ListaDobleEncadenada lista = new ListaDobleEncadenada();
	
	public void setupEscenario1()
	{
		int i = 0;
		while(i < 10)
		{
			lista.agregarElementoFinal("Hola"+i);
			i++;
		}
	}
	
	public void testAgregarVacio()
	{
		lista.agregarElementoFinal("Pepe");
		int a = lista.darNumeroElementos();
		String actual = (String) lista.darElemento(a-1);
		assertEquals("No se agrego el nuevo objeto", "Pepe", actual);
	}
	
	public void testAgregar()
	{
		setupEscenario1();
		lista.agregarElementoFinal("Paco");
		int a = lista.darNumeroElementos();
		String actual = (String) lista.darElemento(a-1);
		assertEquals("No se agrego el nuevo objeto", "Paco", actual);
	}
	
	public void testdarElementos()
	{
		setupEscenario1();
		int a = lista.darNumeroElementos();
		assertEquals("El numero de elementos no corresponde", 10, a);
	}
	
	public void testdarSiguiente()
	{
		setupEscenario1();
		String palabra = (String)lista.darElemento(4);
		
		assertEquals("El elemento no corresponde al esperado segun su poscion", "Hola5", palabra);
		int a = 0;
		while(a < 3)
		{
			lista.avanzarSiguientePosicion();
			a++;
		}
		String xd = (String)lista.darElementoPosicionActual();
		
		assertEquals("El elemento actual no corresponde al esperado", "Hola1", xd);
	}
	
	public void testdarAnterior()
	{
		setupEscenario1();
		
		int a = 0;
		int b = 0;
		
		while(b < 3)
		{
			lista.avanzarSiguientePosicion();
			b++;
		}
		
		while(a < 3)
		{
			lista.retrocederPosicionAnterior();
			a++;
		}
		String xd = (String)lista.darElementoPosicionActual();
		assertEquals("El elemento actual no corresponde al esperado", "Hola0", xd);
	}
	
}
