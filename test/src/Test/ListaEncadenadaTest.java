package Test;
import java.util.ArrayList;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase
{
	private ListaEncadenada lista = new ListaEncadenada();
	
	public void setupEscenario1()
	{
		int i = 0;
		while(i < 10)
		{
			lista.agregarElementoFinal("Hola"+i);
			i++;
		}
	}
	
	public void testAgregarVacio()
	{
		lista.agregarElementoFinal("Pepe");
		int a = lista.darNumeroElementos();
		String actual = (String) lista.darElemento(a-1);
		assertEquals("No se agrego el nuevo objeto", "Pepe", actual);
	}
	
	public void testAgregar()
	{
		setupEscenario1();
		lista.agregarElementoFinal("Paco");
		int a = lista.darNumeroElementos();
		String actual = (String) lista.darElemento(a-1);
		assertEquals("No se agrego el nuevo objeto", "Paco", actual);
	}
	
	public void testdarElementos()
	{
		setupEscenario1();
		int a = lista.darNumeroElementos();
		assertEquals("El numero de elementos no corresponde", 10, a);
	}
	
	public void testdarSiguiente()
	{
		setupEscenario1();
		String palabra = (String)lista.darElemento(4);
		assertEquals("El elemento no corresponde al esperado segun su poscion", "Hola4", palabra);
		int a = 0;
		while(a < 3)
		{
			lista.avanzarSiguientePosicion();
			a++;
		}
		assertEquals("El elemento actual no corresponde al esperado", "Hola1", lista.darElementoPosicionActual());
	}
	
	public void testdarAnterior()
	{
		setupEscenario1();
		
		int a = 0;
		int b = 0;
		
		while(b < 4)
		{
			lista.avanzarSiguientePosicion();
			b++;
		}
		
		while(a < 4)
		{
			lista.retrocederPosicionAnterior();
			a++;
		}
		
		assertEquals("El elemento actual no corresponde al esperado", "Hola0", lista.darElementoPosicionActual());
	}
}
